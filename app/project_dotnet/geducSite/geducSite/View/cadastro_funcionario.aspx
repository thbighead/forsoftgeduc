﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="cadastro_funcionario.aspx.cs" Inherits="geducSite.View.cadastro_funcionario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
     <form id="Formulario" runat="server">
            <h1>Funcion&aacute;rio </h1>
            <h2>Dados Pessoais </h2>

            <asp:Label ID="lblNomeFuncionario" runat="server" Text="Nome:"></asp:Label>
            <asp:TextBox ID="txtNomeFuncionario" MaxLength="50" runat="server"></asp:TextBox>

            <br />
            <br />

            <asp:Label ID="lblDataNascimento" runat="server" Text="Data de Nascimento:"></asp:Label>
            <asp:TextBox ID="txtDataNascimento" MaxLength="50" runat="server"></asp:TextBox>
            <br />
            <br />

            <asp:Label ID="lblSexo" runat="server" Text="Sexo:"></asp:Label>

            <asp:RadioButton ID="rbSexo" Value="Masculino" Checked="true" runat="server" GroupName="sexo" Text="Masculino" />
            <asp:RadioButton ID="rbSexo1" Value="Feminino"  Checked="false" runat="server" GroupName="sexo" Text="Feminino" />


            <br />
            <br />

            <asp:Label ID="lblNaturalidade" runat="server" Text="Naturalidade:"></asp:Label>
            <asp:TextBox ID="txtNaturalidade" MaxLength="50" runat="server"></asp:TextBox>

            <br />
            <br />
            <asp:Label ID="lblNacionalidade" runat="server" Text="Nacionalidade:"></asp:Label>
            <asp:TextBox ID="txtNacionalidade" MaxLength="50" runat="server"></asp:TextBox>

            <br />
            <br />

            <asp:Label ID="lblNomePai" runat="server" Text="Nome do Pai:"></asp:Label>
            <asp:TextBox ID="txtNomePai" MaxLength="50" runat="server"></asp:TextBox>

            <br />
            <br />

            <asp:Label ID="lblNomeMae" runat="server" Text="Nome da M&atilde;e:"></asp:Label>
            <asp:TextBox ID="txtNomeMae" MaxLength="50" runat="server"></asp:TextBox>

            <br />
            <br />

            <asp:Label ID="lblEtnia" runat="server" Text="Etnia:"></asp:Label>
            <asp:DropDownList ID="ddlEtnia" runat="server" CssClass="width-50">
                <asp:ListItem Text="Selecione uma Op&ccedil;&atilde;o" Value="selecione" />
                <asp:ListItem Text="Branco" Value="branco" />
                <asp:ListItem Text="Preta" Value="preta" />
                <asp:ListItem Text="Amarela" Value="amarela" />
                <asp:ListItem Text="Pardo" Value="pardo" />
                <asp:ListItem Text="Ind&iacute;gena" Value="pardo" />
                <asp:ListItem Text="Sem Declara&ccedil;&atilde;o" Value="sem" />

            </asp:DropDownList>

            <br />
            <br />

            <asp:Label ID="lblEstadoCivil" runat="server" Text="Estado Civil:"></asp:Label>
            <asp:DropDownList ID="ddlEstadoCivil" runat="server" CssClass="width-50">
                <asp:ListItem Text="Selecione uma Op&ccedil;&atilde;o" Value="selecione" />
                <asp:ListItem Text="Solteiro(a)" Value="solteiro" />
                <asp:ListItem Text="Casado(a)" Value="casado" />
                <asp:ListItem Text="Divorciado(a)" Value="divorciado" />
                <asp:ListItem Text="Vi&uacute;vo (a)" Value="viuvo" />

            </asp:DropDownList>

            <br />
            <br />

            <asp:Label ID="lblEscolaridade" runat="server" Text="N&iacute;vel de Escolaridade::"></asp:Label>
            <asp:DropDownList ID="ddlEscolaridade" runat="server" CssClass="width-50">
                <asp:ListItem Text="Selecione uma Op&ccedil;&atilde;o" Value="selecione" />
                <asp:ListItem Text="Superior" Value="superior" />
                <asp:ListItem Text="Superior Incompleto" Value="superioin" />
                <asp:ListItem Text="T&eacute;cnico" Value="tecnico" />
                <asp:ListItem Text="T&eacute;cnico Incompleto (a)" Value="tecnicoin" />
                <asp:ListItem Text="M&eacute;dio" Value="medio" />
                <asp:ListItem Text="Vi&M&eacute;dio Incompleto" Value="medioin" />
                <asp:ListItem Text="Fundamental" Value="fundamental" />
                <asp:ListItem Text="Fundamental Incompleto" Value="fundamentalin" />

            </asp:DropDownList>



            <br />
            <br />

            <asp:Label ID="lblNecessidadeEspecial" runat="server" Text="Possui alguma necessidade especial?:"></asp:Label>

            <asp:RadioButton ID="rbNecessidadeSim" Checked="true" runat="server" GroupName="necessidade" Text="Sim" />
            <asp:RadioButton ID="rbNecessidadeNao" Checked="false" runat="server" GroupName="necessidade" Text="Não" />


            <br />
            <br />

            <h2 class="#">Funcion&aacute;rio</h2>

            <asp:Label ID="lblcargo" runat="server" Text="Cargo:"></asp:Label>
            <asp:TextBox ID="cargo" CssClass="#" MaxLength="50" runat="server"></asp:TextBox>

            <br />
            <br />

            <asp:Label ID="lblSituacao" runat="server" Text="Situa&ccedil;&atilde;o:"></asp:Label>
            <asp:DropDownList ID="ddlSituacao" runat="server" CssClass="width-50">
                <asp:ListItem Text="Selecione uma Op&ccedil;&atilde;o" Value="selecione" />
                <asp:ListItem Text="Ativo" Value="ativo" />
                <asp:ListItem Text="Inativo)" Value="inativo" />





            </asp:DropDownList>

            <br />
            <br />

            <h2>Documenta&ccedil;ao </h2>
            <asp:Label ID="lblCpf" runat="server" Text="CPF:"></asp:Label>
            <asp:TextBox ID="txtCpf" runat="server"></asp:TextBox>
            <br />
            <br />

            <asp:Label ID="lblRg" runat="server" Text="RG:"></asp:Label>
            <asp:TextBox ID="txtrg" runat="server"></asp:TextBox>
            <br />
            <br />

            <asp:Label ID="lblExpedido" runat="server" Text="Data de Expedi&ccedil;&atilde;o:"></asp:Label>
            <asp:TextBox ID="txtExpedido" runat="server"></asp:TextBox>
            <br />
            <br />

            <asp:Label ID="lblOrgao" runat="server" Text="Org&atilde;o Expedidor:"></asp:Label>
            <asp:TextBox ID="txtOrgao" runat="server"></asp:TextBox>
            <br />
            <br />

            <p>Certid&atilde;o de Nascimento:</p>

            <asp:Label ID="lblNumCertidaoNascimento" runat="server" Text="N&uacute;mero:"></asp:Label>
            <asp:TextBox ID="txtNumCertificadoNascimento" runat="server"></asp:TextBox>
            <br />
            <br />

            <asp:Label ID="lblLivroCertidaoNascimento" runat="server" Text="Livro"></asp:Label>
            <asp:TextBox ID="txtLivroCertidaoNascimento" runat="server"></asp:TextBox>
            <br />
            <br />

            <asp:Label ID="lblFolhaCertidaoNascimento" runat="server" Text="Folha:"></asp:Label>
            <asp:TextBox ID="txtFolhaCertidaoNascimento" runat="server"></asp:TextBox>
            <br />
            <br />

            <asp:Label ID="lblDataCertidaoNascimento" runat="server" Text="Data de Emiss&atilde;o:"></asp:Label>
            <asp:TextBox ID="txtDataCertidaoNascimento" runat="server"></asp:TextBox>
            <br />
            <br />
            <br />

            <asp:Label ID="lblTituloEleitor" runat="server" Text="T&iacute;tulo de Eleitor:"></asp:Label>
            <asp:TextBox ID="txtTituloEleito" runat="server"></asp:TextBox>
            <br />
            <br />

            <asp:Label ID="lblCertificadoReservista" runat="server" Text="Certificado de Reservista:"></asp:Label>
            <asp:TextBox ID="txtCertificadoReservista" runat="server"></asp:TextBox>
            <br />
            <br />

            <h2>Endere&ccedil;o </h2>

            <asp:Label ID="lblLogradouro" runat="server" Text="Logradouro:"></asp:Label>
            <asp:TextBox ID="txtLogradouro" runat="server"></asp:TextBox>
            <br />
            <br />

            <asp:Label ID="lblNumero" runat="server" Text="N&uacute;mero:"></asp:Label>
            <asp:TextBox ID="txtNumero" runat="server"></asp:TextBox>
            <br />
            <br />

            <asp:Label ID="lblComplemento" runat="server" Text="Complemento:"></asp:Label>
            <asp:TextBox ID="txtComplemento" runat="server"></asp:TextBox>
            <br />
            <br />

            <asp:Label ID="lblBairro" runat="server" Text="Bairro:"></asp:Label>
            <asp:TextBox ID="txtBairro" runat="server"></asp:TextBox>
            <br />
            <br />

            <asp:Label ID="lblCEP" runat="server" Text="CEP:"></asp:Label>
            <asp:TextBox ID="txtCEP" runat="server"></asp:TextBox>
            <br />
            <br />

            <asp:Label ID="lbl" runat="server" Text="UF:"></asp:Label>
            <asp:DropDownList ID="ddlUF" runat="server">
                <asp:ListItem Value=" ">  </asp:ListItem>
                <asp:ListItem Value="1">AC</asp:ListItem>
                <asp:ListItem Value="2">AL</asp:ListItem>
                <asp:ListItem Value="3">AP</asp:ListItem>
                <asp:ListItem Value="4">AM</asp:ListItem>
                <asp:ListItem Value="5">BA</asp:ListItem>
                <asp:ListItem Value="6">CE</asp:ListItem>
                <asp:ListItem Value="7">DF</asp:ListItem>
                <asp:ListItem Value="8">ES</asp:ListItem>
                <asp:ListItem Value="9">GO</asp:ListItem>
                <asp:ListItem Value="10">MA</asp:ListItem>
                <asp:ListItem Value="11">MT</asp:ListItem>
                <asp:ListItem Value="12">MS</asp:ListItem>
                <asp:ListItem Value="13">MG</asp:ListItem>
                <asp:ListItem Value="14">PA</asp:ListItem>
                <asp:ListItem Value="15">PB</asp:ListItem>
                <asp:ListItem Value="16">PR</asp:ListItem>
                <asp:ListItem Value="17">PE</asp:ListItem>
                <asp:ListItem Value="18">PI</asp:ListItem>
                <asp:ListItem Value="19">RJ</asp:ListItem>
                <asp:ListItem Value="20">RN</asp:ListItem>
                <asp:ListItem Value="21">RS</asp:ListItem>
                <asp:ListItem Value="22">RO</asp:ListItem>
                <asp:ListItem Value="23">RR</asp:ListItem>
                <asp:ListItem Value="24">SC</asp:ListItem>
                <asp:ListItem Value="25">SP</asp:ListItem>
                <asp:ListItem Value="26">SE</asp:ListItem>
                <asp:ListItem Value="27">TO</asp:ListItem>
            </asp:DropDownList>
            <br />
            <br />

            <asp:Label ID="lblMunicipio" runat="server" Text="Munic&iacute;pio:"></asp:Label>
            <asp:TextBox ID="txtMunicipio" runat="server"></asp:TextBox>
            <br />
            <br />

            <asp:Label ID="lblZona" runat="server" Text="Zona:"></asp:Label>
            <asp:TextBox ID="txtZona" runat="server"></asp:TextBox>
            <br />
            <br />


            <h2>Contato </h2>

            <asp:Label ID="lblTelefone" runat="server" Text="Telefone:"></asp:Label>
            <asp:TextBox ID="txtTelefone" runat="server"></asp:TextBox>
            <br />
            <br />

            <asp:Label ID="lblCelular" runat="server" Text="Celular:"></asp:Label>
            <asp:TextBox ID="txtCelular" runat="server"></asp:TextBox>
            <br />
            <br />

            <asp:Label ID="lblEmail" runat="server" Text="E-mail:"></asp:Label>
            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
            <br />
            <br />

            <asp:Label ID="lblOutros" runat="server" Text="Outros:"></asp:Label>
            <asp:TextBox ID="txtOutros" runat="server"></asp:TextBox>
            <br />
            <br />

            <h2>Programa Social </h2>

            <asp:Label ID="lblProgramaSocial" runat="server" Text="Participa de algum programa social"></asp:Label>
            <asp:RadioButton ID="rbProgramaSocialN" runat="server" Checked="true" GroupName="ProgramaSocial" Text="N&atilde;o" />
            <asp:RadioButton ID="rbProgramaSocialS" runat="server" Checked="false" GroupName="ProgramaSocial" Text="Sim" />
            <br />
            <br />
            <br />

            <asp:Button ID="btnVoltar" runat="server" Text="Voltar" />
            <asp:Button ID="btnAlterar" runat="server" Text="Alterar" />
            <asp:Button ID="btnLimpar" runat="server" Text="Limpar" />
            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" />
            <asp:Button ID="Cadastrar" runat="server" Text="Cadastrar" OnClick="Cadastrar_Click" />
            <asp:Button ID="Desativar" runat="server" Text="Desativar" />
            <br />
            <br />
         
    <asp:GridView ID="grdFuncionario" runat="server" />
        </form>
</asp:Content>
