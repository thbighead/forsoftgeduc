﻿using geducSite.Context;
using geducSite.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace geducSite.Persistence
{
    public class DisciplinaDAO : Conexao
    {
        public void Cadastrar(Disciplina d) 
        {
            try
            {
                AbrirConexao();
                cmd = new SqlCommand("INSERT INTO disciplina VALUES(@v1,@v2,@v3) ",con);
                cmd.Parameters.AddWithValue("@v1", d.nome);
                cmd.Parameters.AddWithValue("@v2", d.codigo);
                cmd.Parameters.AddWithValue("@v3", d.cargaHoraria);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                FecharConexao();
            }
        }

        public void Deletar(Disciplina d)
        {
            try
            {
                AbrirConexao();
                cmd = new SqlCommand("DELETE FROM disciplina WHERE idDisciplina = @v1 ", con);
                cmd.Parameters.AddWithValue("@v1", d.idDisciplina);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                FecharConexao();
            }
        }

        public void Atualizar(Disciplina d)
        {
            try
            {
                AbrirConexao();
                cmd = new SqlCommand("UPDATE disciplina SET nome = @v1, codigo= @v2 ,cargaHoraria = @v3 WHERE idDisciplina = @v4  ", con);
                cmd.Parameters.AddWithValue("@v1", d.nome);
                cmd.Parameters.AddWithValue("@v2", d.codigo);
                cmd.Parameters.AddWithValue("@v3", d.cargaHoraria);
                cmd.Parameters.AddWithValue("@v4", d.idDisciplina);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                FecharConexao();
            }
        }

        public List<Disciplina> ListarDisciplinas()
        {
            try 
            {
                AbrirConexao();
                cmd = new SqlCommand("SELECT * FROM disciplina ORDER BY nome",con);
                dr = cmd.ExecuteReader();

                List<Disciplina> lista = new List<Disciplina>();

                while (dr.Read()) 
                {
                    Disciplina d = new Disciplina();
                    d.idDisciplina = Convert.ToInt32(dr["idDisciplina"]);
                    d.nome = Convert.ToString(dr["nome"]);
                    d.codigo = Convert.ToString(dr["codigo"]);
                    d.cargaHoraria = Convert.ToInt32(dr["cargaHoraria"]);
                    lista.Add(d);
                }
                return lista;
            }
            finally 
            {
                FecharConexao();
            }
        }

        public Disciplina BuscarDisciplina(int id)
        {
            try
            {
                AbrirConexao();

                cmd = new SqlCommand("SELECT * FROM disciplina WHERE idDisciplina = @v1", con);
                cmd.Parameters.AddWithValue("@v1",id);
                dr = cmd.ExecuteReader();

                Disciplina d = new Disciplina();
                
                while (dr.Read())
                {
                    d.idDisciplina = Convert.ToInt32(dr["idDisciplina"]);
                    d.nome = Convert.ToString(dr["nome"]);
                    d.codigo = Convert.ToString(dr["codigo"]);
                    d.cargaHoraria = Convert.ToInt32(dr["cargaHoraria"]);
                }

                return d;
            }
            finally
            {
                FecharConexao();
            }
        }
    }
}