﻿using geducSite.Context;
using geducSite.MODEL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace geducSite.Persistence
{
    public class FuncionarioDAO : Conexao
    {
        //Método para gravar o usuario na base de dados...
        public void Salvar(Funcionario f)
        {
            try
            {
                AbrirConexao();
                //Gravar Contato
                cmd = new SqlCommand("insert into contato(telefoneFixo, telefoneCelular,email, outros) values(@v1, @v2, @v3, @v4) SELECT SCOPE_IDENTITY()", con);
                cmd.Parameters.AddWithValue("@v1", f.contato.telefone);
                cmd.Parameters.AddWithValue("@v2", f.contato.celular);
                cmd.Parameters.AddWithValue("@v3", f.contato.email);
                cmd.Parameters.AddWithValue("@v4", f.contato.outros);
                f.contato.idContato = Convert.ToInt32(cmd.ExecuteScalar());

                //Documento
                cmd = new SqlCommand("insert into documento(cpf,rg,dataExpedicao,orgaoExpedidor,numCertidao,livroCertidao,folhaCertidao,dataEmiCertidao,titEleitor,certReservista) values(@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9, @v10) SELECT SCOPE_IDENTITY()", con);
                cmd.Parameters.AddWithValue("@v1", f.documento.certReservista);
                cmd.Parameters.AddWithValue("@v2", f.documento.rg);
                cmd.Parameters.AddWithValue("@v3", f.documento.dataExpedicao);
                cmd.Parameters.AddWithValue("@v4", f.documento.orgaoExpedidor);
                cmd.Parameters.AddWithValue("@v5", f.documento.numCertidao);
                cmd.Parameters.AddWithValue("@v6", f.documento.livroCertidao);
                cmd.Parameters.AddWithValue("@v7", f.documento.folhaCertidao);
                cmd.Parameters.AddWithValue("@v8", f.documento.dataEmiCertidao);
                cmd.Parameters.AddWithValue("@v9", f.documento.titEleitor);
                cmd.Parameters.AddWithValue("@v10", f.documento.certReservista);
                f.documento.idDocumento = Convert.ToInt32(cmd.ExecuteScalar());

                //Cargo
                cmd = new SqlCommand("insert into cargo values(@v1, @v2) SELECT SCOPE_IDENTITY()", con);
                cmd.Parameters.AddWithValue("@v1", f.cargo.cargo);
                cmd.Parameters.AddWithValue("@v2", "nulo");
                f.cargo.idCargo = Convert.ToInt32(cmd.ExecuteScalar());

                //gravar o endereço
                cmd = new SqlCommand("insert into endereco(logradouro, numero, complemento, bairro, cidade, uf, cep,municipio, zona) values(@v1, @v2, @v3, @v4, @v5,@v6,@v7,@v8,@v9) SELECT SCOPE_IDENTITY()", con);
                cmd.Parameters.AddWithValue("@v1", f.endereco.Longradouro);
                cmd.Parameters.AddWithValue("@v2", f.endereco.numero);
                cmd.Parameters.AddWithValue("@v3", f.endereco.complemento);
                cmd.Parameters.AddWithValue("@v4", f.endereco.bairro);
                cmd.Parameters.AddWithValue("@v5", "nulo");
                cmd.Parameters.AddWithValue("@v6", f.endereco.uf);
                cmd.Parameters.AddWithValue("@v7", f.endereco.cep);
                cmd.Parameters.AddWithValue("@v8", f.endereco.municipio);
                cmd.Parameters.AddWithValue("@v9", f.endereco.zona);
                f.endereco.idEndereco = Convert.ToInt32(cmd.ExecuteScalar());

                //apenas para testes e a apresentação de sexta
                cmd = new SqlCommand("insert into login values('login', 'senha', 'tipo')SELECT SCOPE_IDENTITY()", con);
                int idLogin = Convert.ToInt32(cmd.ExecuteScalar());
                //apenas para testes e a apresentação de sexta
                cmd = new SqlCommand("insert into programaSocial values('nome', 'descricao', 'adm')SELECT SCOPE_IDENTITY()", con);
                int idProgramaSocial = Convert.ToInt32(cmd.ExecuteScalar());

                //Pessoa
                cmd = new SqlCommand("INSERT INTO pessoa (nome, dataNascimento, sexo, naturalidade, nacionalidade, nomePai, nomeMae, etnia, estadoCivil, nivelEscolaridade, necessidadeEsp, idLogin, idProgramaSocial, idContato, idEndereco, idDocumento) VALUES(@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9, @v10, @v11, @v12, @v13, @v14, @v15, @v16) SELECT SCOPE_IDENTITY()", con);
                cmd.Parameters.AddWithValue("@v1", f.nome);
                cmd.Parameters.AddWithValue("@v2", f.dataNascimento);
                cmd.Parameters.AddWithValue("@v3", f.sexo);
                cmd.Parameters.AddWithValue("@v4", f.naturalidade);
                cmd.Parameters.AddWithValue("@v5", f.nacionalidade);
                cmd.Parameters.AddWithValue("@v6", f.nomePai);
                cmd.Parameters.AddWithValue("@v7", f.nomeMae);
                cmd.Parameters.AddWithValue("@v8", f.etnia);
                cmd.Parameters.AddWithValue("@v9", f.estadoCivil);
                cmd.Parameters.AddWithValue("@v10", f.nivelEscolaridade);
                cmd.Parameters.AddWithValue("@v11", f.necessidadeEsp);
                cmd.Parameters.AddWithValue("@v12", idLogin);
                cmd.Parameters.AddWithValue("@v13", idProgramaSocial);
                cmd.Parameters.AddWithValue("@v14", f.contato.idContato);
                cmd.Parameters.AddWithValue("@v15", f.endereco.idEndereco);
                cmd.Parameters.AddWithValue("@v16", f.documento.idDocumento);
                f.idPessoa = Convert.ToInt32(cmd.ExecuteScalar());

                //gravar o funcionario junto com pessoa, código precisa ser melhorado, mas para ter um noção tá valendo
                cmd = new SqlCommand("insert into funcionario(situacao,idCargo,idPessoa)values(@v1,@v2,@v3)", con);
                cmd.Parameters.AddWithValue("@v1", f.situacao);
                cmd.Parameters.AddWithValue("@v2", f.cargo.idCargo);
                cmd.Parameters.AddWithValue("@v3", f.idPessoa);
                cmd.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

        public List<Funcionario> listar() {
            try {
                AbrirConexao();
                cmd = new SqlCommand("select * from pessoa",con);
                dr = cmd.ExecuteReader();

                List<Funcionario> lista = new List<Funcionario>();

                while (dr.Read()) {
                    Funcionario f = new Funcionario();
                    f.idPessoa = Convert.ToInt32(dr["idPessoa"]);
                    f.dataNascimento = Convert.ToDateTime(dr["dataNascimento"]);
                    f.sexo = Convert.ToString(dr["sexo"]);
                    f.naturalidade = Convert.ToString(dr["naturalidade"]);
                    f.nacionalidade = Convert.ToString(dr["nacionalidade"]);
                    f.nomePai = Convert.ToString(dr["nomePai"]);
                    f.nomeMae = Convert.ToString(dr["nomeMae"]);
                    f.etnia = Convert.ToString(dr["etnia"]);
                    f.estadoCivil = Convert.ToString(dr["estadoCivil"]);
                    f.nivelEscolaridade = Convert.ToString(dr["nivelEscolaridade"]);
                    int idLogin = Convert.ToInt32(dr["idLogin"]);
                    int idPtrogramaSocial = Convert.ToInt32(dr["idPtrogramaSocial"]);
                    f.endereco.idEndereco = Convert.ToInt32(dr["idEndereco"]);
                    f.contato.idContato = Convert.ToInt32(dr["idContato"]);
                    f.documento.idDocumento = Convert.ToInt32(dr["idDocumento"]);
                    lista.Add(f);
                }
                return lista;
            }
            finally {
                FecharConexao();    
            }
        }
    }
}