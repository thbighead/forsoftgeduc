﻿using geducSite.Context;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace geducSite.Persistence
{
    public class NotaDAO : Conexao
    {
        public void cadastrarNota(Nota n)
        {
            try
            {
                AbrirConexao();
                cmd = new SqlCommand("insert into nota values(@v1,@v2,@v3,@v4,@v5,@v6)", con);
                cmd.Parameters.AddWithValue("@v1", n.aluno.idAluno);
                cmd.Parameters.AddWithValue("@v2", n.disciplina.idDisciplina);
                cmd.Parameters.AddWithValue("@v3", n.nota);
                cmd.Parameters.AddWithValue("@v4", n.periodo);
                cmd.Parameters.AddWithValue("@v5", n.origim);
                cmd.Parameters.AddWithValue("@v6", n.data);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                FecharConexao();
            }
        }

    }
}