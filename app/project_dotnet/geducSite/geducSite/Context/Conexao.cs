﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace geducSite.Context
{
    public class Conexao
    {
        protected SqlConnection con;
        protected SqlCommand cmd;
        protected SqlDataReader dr;

        protected void AbrirConexao()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["conexao"].ConnectionString);
            con.Open();
        }

        protected void FecharConexao()
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }
}