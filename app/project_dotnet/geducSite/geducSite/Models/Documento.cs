﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace geducSite.MODEL
{
    public class Documento
    {
        public int idDocumento { get; set; }
        public string cpf { get; set; }
        public string rg { get; set; }
        public DateTime dataExpedicao { get; set; }
        public string orgaoExpedidor { get; set; }
        public string numCertidao { get; set; }
        public string livroCertidao { get; set; }
        public string folhaCertidao { get; set; }
        public DateTime dataEmiCertidao { get; set; }
        public string titEleitor { get; set; }
        public string certReservista { get; set; }
    }
}
