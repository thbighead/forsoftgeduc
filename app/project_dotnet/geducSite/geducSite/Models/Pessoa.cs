﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace geducSite.MODEL
{
    public class Pessoa
    {
        /// <summary>
        /// atributos da classe pessoao 
        /// ps:lembrando que os atributos são apenas hipotéticos 
        /// </summary>
        public int idPessoa { get; set; }
        public string nome { get; set; }
        public DateTime dataNascimento { get; set; }
        public string sexo { get; set; }
        public string naturalidade { get; set; }
        public string nacionalidade { get; set; }
        public string nomePai { get; set; }
        public string nomeMae { get; set; }
        public string etnia { get; set; }
        public string estadoCivil { get; set; }
        public string nivelEscolaridade { get; set; }
        public string necessidadeEsp { get; set; }
        /// <summary>
        /// a classe endereco e contato estar dentro da classe pessoa dizendo que o relacionamento entre as mesma é de um para um
        /// caso fosse de um para n, teria que usar um list
        /// 
        /// <example>
        /// public list"Endereco" endereco {get; set;}
        /// </example>
        /// </summary>
        public Endereco endereco { get; set; }
        public Contato contato { get; set; }
        public Documento documento { get; set; }
    }
}
